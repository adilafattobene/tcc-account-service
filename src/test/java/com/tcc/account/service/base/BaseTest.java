package com.tcc.account.service.base;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.jupiter.api.BeforeAll;

public abstract class BaseTest {

    @BeforeAll
    public static void init() {
        FixtureFactoryLoader.loadTemplates("com.tcc.account.service.templates");
    }
}
