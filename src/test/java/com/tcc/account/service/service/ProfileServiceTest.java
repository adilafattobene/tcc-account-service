package com.tcc.account.service.service;

import br.com.six2six.fixturefactory.Fixture;
import com.tcc.account.service.base.BaseTest;
import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.ProfileDAO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.ProfileDTO;
import com.tcc.account.service.repository.ProfileRepository;
import com.tcc.account.service.templates.LoginDTOTemplate;
import com.tcc.account.service.templates.ProfileDAOTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ProfileServiceTest extends BaseTest {

    private static final UUID PROFILE_ID_VALID = UUID.randomUUID();
    private static final UUID PROFILE_ID_INVALID = UUID.randomUUID();
    public static final UUID USER_ID = UUID.randomUUID();
    private ProfileService profileService;

    @Mock
    private ProfileRepository profileRepository;

    @Mock
    private LoginService loginService;

    @BeforeEach
    void setUp() {
        openMocks(this);

        profileService = new ProfileService(profileRepository, loginService);
    }

    @Test
    void should_return_a_profile_list() {

        when(profileRepository.findAll())
                .thenReturn(Fixture.from(ProfileDAO.class).gimme(3, ProfileDAOTemplate.TYPES.VALID.name()));

        List<ProfileDTO> profiles = profileService.getProfiles();

        assertThat(profiles).isNotNull().isNotEmpty();
    }

    @Test
    void should_return_a_empty_list() {

        when(profileRepository.findAll())
                .thenReturn(Collections.emptyList());

        List<ProfileDTO> profiles = profileService.getProfiles();

        assertThat(profiles).isEmpty();
    }

    @Test
    void should_return_a_profile_when_it_exists() {
        ProfileDAO profileDAO = Fixture.from(ProfileDAO.class).gimme(ProfileDAOTemplate.TYPES.VALID.name());
        when(profileRepository.findById(PROFILE_ID_VALID))
                .thenReturn(Optional.of(profileDAO));

        ProfileDTO profileById = profileService.getProfileById(PROFILE_ID_VALID);

        assertThat(profileById).isNotNull();
        assertThat(profileById.getId())
                .isNotNull()
                .isEqualTo(profileDAO.getId());
        assertThat(profileById.getDescription())
                .isNotNull()
                .isEqualTo(profileDAO.getDescription());
    }

    @Test
    void should_throw_an_exception_when_profile_does_not_exist() {
        when(profileRepository.findById(PROFILE_ID_VALID))
                .thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> profileService.getProfileById(PROFILE_ID_INVALID));
    }

    @Test
    void should_return_a_profile_when_user_id_exists() {
        ProfileDAO profileDAO = Fixture.from(ProfileDAO.class).gimme(ProfileDAOTemplate.TYPES.VALID.name());
        LoginDTO loginDTO = Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        ProfileDTO userProfile = loginDTO.getProfile();

        when(profileRepository.findById(PROFILE_ID_VALID))
                .thenReturn(Optional.of(profileDAO));
        when(loginService.getLoginById(USER_ID))
                .thenReturn(loginDTO);

        ProfileDTO profileById = profileService.getUserProfileByUserId(USER_ID);

        assertThat(profileById).isNotNull();
        assertThat(profileById.getId())
                .isNotNull()
                .isEqualTo(userProfile.getId());
        assertThat(profileById.getDescription())
                .isNotNull()
                .isEqualTo(userProfile.getDescription());
    }

    @Test
    void should_throw_an_exception_when_user_does_not_exist() {
        when(loginService.getLoginById(USER_ID))
                .thenReturn(null);

        assertThrows(NotFoundException.class, () -> profileService.getUserProfileByUserId(USER_ID));
    }
}