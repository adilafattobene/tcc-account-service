package com.tcc.account.service.service;

import br.com.six2six.fixturefactory.Fixture;
import com.tcc.account.service.base.BaseTest;
import com.tcc.account.service.controller.exceptions.ConflictException;
import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.AccountDAO;
import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.SimpleUserDTO;
import com.tcc.account.service.model.dto.UserDTO;
import com.tcc.account.service.repository.AccountRepository;
import com.tcc.account.service.templates.AccountDAOTemplate;
import com.tcc.account.service.templates.CompanyDTOTemplate;
import com.tcc.account.service.templates.LoginDTOTemplate;
import com.tcc.account.service.templates.UserDTOTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserServiceTest extends BaseTest {

    private UserService userService;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private CompanyService companyService;
    @Mock
    private LoginService loginService;

    private static final UUID USER_OWNER_ID = UUID.randomUUID();
    private static final UUID USER_ID_NONEXISTENT = UUID.randomUUID();

    @BeforeEach
    void setUp() {
        openMocks(this);

        userService = new UserService(accountRepository, companyService, loginService);
    }

    @Test
    void should_return_an_user_with_full_params_when_it_exists() {

        AccountDAO accountDAO = Fixture.from(AccountDAO.class).gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());
        when(accountRepository.findById(USER_OWNER_ID))
                .thenReturn(Optional.of(accountDAO));

        SimpleUserDTO user = userService.getUserBy(USER_OWNER_ID);

        assertThat(user.getId())
                .isNotNull()
                .isEqualTo(accountDAO.getId());
        assertThat(user.getName())
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getName());
        assertThat(user.getCompany()).isNotNull();
        assertThat(user.getCompany().getId())
                .isNotNull()
                .isEqualTo(accountDAO.getCompany().getId());
        assertThat(user.getCompany().getName())
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getCompany().getName());
    }

    @Test
    void should_throw_not_found_exception_when_user_does_not_exists() {

        when(accountRepository.findById(USER_ID_NONEXISTENT))
                .thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> userService.getUserBy(USER_ID_NONEXISTENT));
    }

    @Test
    void should_return_an_user_list_when_owner_id_is_a_valid_user() {

        when(accountRepository.findAllUsersById(USER_OWNER_ID))
                .thenReturn(Fixture.from(AccountDAO.class).gimme(2, AccountDAOTemplate.TYPES.VALID_CHILD.name()));
        when(loginService.getLoginById(any(UUID.class)))
                .thenReturn(Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_CHILD_LOGIN.name()));

        List<UserDTO> usersById = userService.getUsersById(USER_OWNER_ID);

        assertThat(usersById).isNotEmpty();
        assertThat(usersById.get(0).getName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(usersById.get(0).getLogin().getUserName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(usersById.get(0).getLogin().getProfile().getId()).isNotNull();
        assertThat(usersById.get(0).getLogin().getProfile().getDescription()).isNotNull().isNotEmpty().isNotBlank();
    }

    @Test
    void should_return_an_empty_list_when_owner_id_is_a_valid_user_but_it_does_not_have_children() {

        when(accountRepository.findAllUsersById(USER_OWNER_ID))
                .thenReturn(Collections.emptyList());

        List<UserDTO> usersById = userService.getUsersById(USER_OWNER_ID);

        assertThat(usersById).isEmpty();
    }

    @Test
    void should_throw_not_found_exception_when_some_child_does_not_have_login() {

        when(accountRepository.findAllUsersById(USER_OWNER_ID))
                .thenReturn(Fixture.from(AccountDAO.class).gimme(2, AccountDAOTemplate.TYPES.VALID_CHILD.name()));
        when(loginService.getLoginById(any(UUID.class)))
                .thenThrow(new NotFoundException("Login não cadastrado"));

        assertThrows(NotFoundException.class, () -> userService.getUsersById(USER_OWNER_ID));
    }

    @Test
    void should_create_correctly_a_new_user_with_owner_profile() {
        UserDTO userFixtureToCreate = Fixture.from(UserDTO.class).gimme(UserDTOTemplate.TYPES.VALID_TO_CREATE_OWNER.name());

        when(companyService.verifyIfCompanyExist(userFixtureToCreate.getCompany().getName()))
                .thenReturn(false);
        when(companyService.createCompany(any(CompanyDTO.class)))
                .thenReturn(Fixture.from(CompanyDTO.class).gimme(CompanyDTOTemplate.TYPES.VALID.name()));
        when(accountRepository.save(any(AccountDAO.class)))
                .thenReturn(Fixture.from(AccountDAO.class).gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name()));
        when(loginService.createLogin(eq(userFixtureToCreate.getLogin()), any(UUID.class)))
                .thenReturn(Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_OWNER_LOGIN.name()));


        UserDTO newUser = userService.createUser(userFixtureToCreate);

        assertThat(newUser).isNotNull();
        assertThat(newUser.getName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(newUser.getCompany().getId()).isNotNull();
        assertThat(newUser.getCompany().getName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(newUser.getLogin()).isNotNull();
        assertThat(newUser.getLogin().getId()).isNotNull();
        assertThat(newUser.getLogin().getPassword()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(newUser.getLogin().getUserName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(newUser.getLogin().getProfile()).isNotNull();
        assertThat(newUser.getLogin().getProfile().getId()).isNotNull();
        assertThat(newUser.getLogin().getProfile().getDescription()).isNotNull().isNotEmpty().isNotBlank();
    }

    @Test
    void should_throw_a_conflict_exception_when_try_to_create_an_user_with_already_company_existing() {
        UserDTO userFixtureToCreate = Fixture.from(UserDTO.class).gimme(UserDTOTemplate.TYPES.VALID_TO_CREATE_OWNER.name());

        when(companyService.verifyIfCompanyExist(userFixtureToCreate.getCompany().getName()))
                .thenReturn(true);

        assertThrows(ConflictException.class, () -> userService.createUser(userFixtureToCreate));
    }

    @Test
    void should_create_new_user_child_correctly() {
        UserDTO userFixtureToCreateChild = Fixture.from(UserDTO.class).gimme(UserDTOTemplate.TYPES.VALID_TO_CREATE_CHILD.name());

        when(accountRepository.findById(USER_OWNER_ID))
                .thenReturn(Optional.of(Fixture.from(AccountDAO.class).gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name())));

        AccountDAO childSavedMock = Fixture.from(AccountDAO.class).gimme(AccountDAOTemplate.TYPES.VALID_CHILD.name());
        when(accountRepository.save(any(AccountDAO.class)))
                .thenReturn(childSavedMock);
        when(loginService.createLogin(any(LoginDTO.class), eq(childSavedMock.getId())))
                .thenReturn(Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_CHILD_LOGIN.name()));


        UserDTO childReturned = userService.createSubUser(userFixtureToCreateChild, USER_OWNER_ID);

        assertThat(childReturned).isNotNull();
        assertThat(childReturned.getId()).isNotNull();
        assertThat(childReturned.getName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(childReturned.getCompany()).isNotNull();
        assertThat(childReturned.getCompany().getId()).isNotNull();
        assertThat(childReturned.getLogin().getId()).isNotNull();
        assertThat(childReturned.getLogin().getPassword()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(childReturned.getLogin().getUserName()).isNotNull().isNotEmpty().isNotBlank();
        assertThat(childReturned.getLogin().getProfile()).isNotNull();
        assertThat(childReturned.getLogin().getProfile().getDescription()).isNotNull().isNotEmpty().isNotBlank();
    }

    @Test
    void should_return_not_found_exception_when_owner_user_id_does_not_exist_into_database() {
        UserDTO userFixtureToCreateChild = Fixture.from(UserDTO.class).gimme(UserDTOTemplate.TYPES.VALID_TO_CREATE_CHILD.name());

        when(accountRepository.findById(USER_OWNER_ID)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> userService.createSubUser(userFixtureToCreateChild, USER_OWNER_ID));
    }

    @Test
    void should_change_correctly_a_user_when_it_receive_all_information_changed() {

        AccountDAO accountDAO = Fixture.from(AccountDAO.class)
                .gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());

        UserDTO userDTO = Fixture.from(UserDTO.class)
                .gimme(UserDTOTemplate.TYPES.VALID.name());

        when(accountRepository.findById(userDTO.getId()))
                .thenReturn(Optional.of(accountDAO));
        when(companyService.changeCompany(userDTO.getCompany()))
                .thenReturn(userDTO.getCompany());
        when(accountRepository.save(accountDAO))
                .thenReturn(accountDAO);

        UserDTO userChanged = userService.changeUser(userDTO, userDTO.getId());

        assertThat(userChanged).isNotNull();
        assertThat(userChanged.getId()).isNotNull()
                .isEqualTo(accountDAO.getId());
        assertThat(userChanged.getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userDTO.getName());
        assertThat(userChanged.getCompany()).isNotNull();
        assertThat(userChanged.getCompany().getId())
                .isNotNull()
                .isEqualTo(userDTO.getCompany().getId());
        assertThat(userChanged.getCompany().getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userDTO.getCompany().getName());
        assertThat(userChanged.getLogin()).isNull();
    }

    @Test
    void should_change_correctly_a_user_when_it_receive_just_the_name_changed() {
        AccountDAO accountDAO = Fixture.from(AccountDAO.class)
                .gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());

        UserDTO userDTO = Fixture.from(UserDTO.class)
                .gimme(UserDTOTemplate.TYPES.VALID_USER_TO_CHANGE_NAME.name());

        when(accountRepository.findById(userDTO.getId()))
                .thenReturn(Optional.of(accountDAO));
        when(companyService.changeCompany(userDTO.getCompany()))
                .thenReturn(userDTO.getCompany());
        when(accountRepository.save(accountDAO))
                .thenReturn(accountDAO);

        UserDTO userChanged = userService.changeUser(userDTO, userDTO.getId());

        assertThat(userChanged).isNotNull();
        assertThat(userChanged.getId()).isNotNull()
                .isEqualTo(accountDAO.getId());
        assertThat(userChanged.getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userDTO.getName());

        assertThat(userChanged.getCompany()).isNotNull();
        assertThat(userChanged.getCompany().getId())
                .isNotNull()
                .isEqualTo(accountDAO.getCompany().getId());
        assertThat(userChanged.getCompany().getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getCompany().getName());

        assertThat(userChanged.getLogin()).isNull();
    }

    @Test
    void should_change_correctly_a_user_when_it_receive_just_the_company_changed() {
        AccountDAO accountDAO = Fixture.from(AccountDAO.class)
                .gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());

        UserDTO userDTO = Fixture.from(UserDTO.class)
                .gimme(UserDTOTemplate.TYPES.VALID_USER_TO_CHANGE_COMPANY.name());

        when(accountRepository.findById(userDTO.getId()))
                .thenReturn(Optional.of(accountDAO));
        when(companyService.changeCompany(userDTO.getCompany()))
                .thenReturn(userDTO.getCompany());
        when(accountRepository.save(accountDAO))
                .thenReturn(accountDAO);

        UserDTO userChanged = userService.changeUser(userDTO, userDTO.getId());

        assertThat(userChanged).isNotNull();
        assertThat(userChanged.getId()).isNotNull()
                .isEqualTo(accountDAO.getId());

        assertThat(userChanged.getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getName());

        assertThat(userChanged.getCompany()).isNotNull();
        assertThat(userChanged.getCompany().getId())
                .isNotNull()
                .isEqualTo(userDTO.getCompany().getId());
        assertThat(userChanged.getCompany().getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userDTO.getCompany().getName());
        assertThat(userChanged.getLogin()).isNull();
    }

    @Test
    void should_change_correctly_a_user_when_it_receive_just_the_login_changed() {
        AccountDAO accountDAO = Fixture.from(AccountDAO.class)
                .gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());

        UserDTO userDTO = Fixture.from(UserDTO.class)
                .gimme(UserDTOTemplate.TYPES.VALID_USER_TO_CHANGE_LOGIN.name());

        when(accountRepository.findById(userDTO.getId()))
                .thenReturn(Optional.of(accountDAO));
        when(companyService.changeCompany(userDTO.getCompany()))
                .thenReturn(userDTO.getCompany());
        when(accountRepository.save(accountDAO))
                .thenReturn(accountDAO);

        UserDTO userChanged = userService.changeUser(userDTO, userDTO.getId());

        assertThat(userChanged).isNotNull();
        assertThat(userChanged.getId()).isNotNull()
                .isEqualTo(accountDAO.getId());

        assertThat(userChanged.getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getName());
        assertThat(userChanged.getCompany()).isNotNull();
        assertThat(userChanged.getCompany().getId())
                .isNotNull()
                .isEqualTo(accountDAO.getCompany().getId());
        assertThat(userChanged.getCompany().getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(accountDAO.getCompany().getName());

        assertThat(userChanged.getLogin()).isNull();
    }

    @Test
    void should_throw_an_error_when_it_try_change_a_user_with_an_invalid_company() {
        AccountDAO accountDAO = Fixture.from(AccountDAO.class)
                .gimme(AccountDAOTemplate.TYPES.VALID_OWNER.name());

        UserDTO userDTO = Fixture.from(UserDTO.class)
                .gimme(UserDTOTemplate.TYPES.INVALID_COMPANY_TO_CHANGE_USER.name());

        when(accountRepository.findById(userDTO.getId()))
                .thenReturn(Optional.of(accountDAO));
        when(companyService.changeCompany(userDTO.getCompany()))
                .thenReturn(userDTO.getCompany());

        assertThrows(IllegalArgumentException.class, () -> userService.changeUser(userDTO, userDTO.getId()));
    }
}