package com.tcc.account.service.service;

import br.com.six2six.fixturefactory.Fixture;
import com.tcc.account.service.base.BaseTest;
import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.LoginDAO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.repository.LoginRepository;
import com.tcc.account.service.templates.LoginDAOTemplate;
import com.tcc.account.service.templates.LoginDTOTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class LoginServiceTest extends BaseTest {

    private static final UUID USER_ID = UUID.randomUUID();
    private static final String USER_NAME_INVALID = "userNameValid";
    private static final UUID USER_ID_INVALID = UUID.randomUUID();
    private LoginService loginService;

    @Mock
    private LoginRepository loginRepository;

    @BeforeEach
    void setUp() {
        openMocks(this);

        loginService = new LoginService(loginRepository);
    }

    @Test
    void should_return_a_login_when_it_exists() {

        LoginDAO loginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        when(loginRepository.findBy(USER_ID))
                .thenReturn(loginDAO);

        LoginDTO loginById = loginService.getLoginById(USER_ID);

        assertThat(loginById).isNotNull();
        assertThat(loginById.getId())
                .isNotNull()
                .isEqualTo(loginDAO.getId());
        assertThat(loginById.getUserName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getUserName());
        assertThat(loginById.getPassword()).isNull();
        assertThat(loginById.getProfile()).isNotNull();
        assertThat(loginById.getProfile()
                .getId())
                .isNotNull()
                .isEqualTo(loginDAO.getProfile().getId());
        assertThat(loginById.getProfile().getDescription())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getProfile().getDescription());
    }

    @Test
    void should_throw_a_not_found_exception_when_login_does_not_exist() {

        when(loginRepository.findBy(USER_ID))
                .thenReturn(null);

        assertThrows(NotFoundException.class, () -> loginService.getLoginById(USER_ID));
    }

    @Test
    void should_create_owner_login_correctly() {

        LoginDAO loginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        when(loginRepository.save(any(LoginDAO.class)))
                .thenReturn(loginDAO);

        LoginDTO login = loginService
                .createLogin(Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_TO_CREATE_OWNER.name()),
                        USER_ID);

        assertThat(login).isNotNull();
        assertThat(login.getId())
                .isNotNull()
                .isEqualTo(loginDAO.getId());
        assertThat(login.getUserName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getUserName());
        assertThat(login.getPassword()).isNull();
        assertThat(login.getProfile()).isNotNull();
        assertThat(login.getProfile()
                .getId())
                .isNotNull()
                .isEqualTo(loginDAO.getProfile().getId());
        assertThat(login.getProfile().getDescription())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getProfile().getDescription());
    }

    @Test
    void should_create_child_login_correctly() {

        LoginDAO userChildLoginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_CHILD_LOGIN.name());
        when(loginRepository.save(any(LoginDAO.class)))
                .thenReturn(userChildLoginDAO);

        LoginDTO login = loginService
                .createLogin(Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_TO_CREATE.name()),
                        USER_ID);

        assertThat(login).isNotNull();
        assertThat(login.getId())
                .isNotNull()
                .isEqualTo(userChildLoginDAO.getId());
        assertThat(login.getUserName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userChildLoginDAO.getUserName());
        assertThat(login.getPassword()).isNull();
        assertThat(login.getProfile()).isNotNull();
        assertThat(login.getProfile()
                .getId())
                .isNotNull()
                .isEqualTo(userChildLoginDAO.getProfile().getId());
        assertThat(login.getProfile().getDescription())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(userChildLoginDAO.getProfile().getDescription());
    }

    @Test
    void should_change_login_when_profile_was_not_change() {

        LoginDAO loginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(loginDAO);
        when(loginRepository.save(any(LoginDAO.class)))
                .thenReturn(loginDAO);

        LoginDTO loginChanged = loginService.changeLogin(USER_ID,
                Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_OWNER_LOGIN.name()));

        assertThat(loginChanged).isNotNull();
        assertThat(loginChanged.getId())
                .isNotNull()
                .isEqualTo(loginDAO.getId());
        assertThat(loginChanged.getUserName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getUserName());
        assertThat(loginChanged.getPassword()).isNull();
        assertThat(loginChanged.getProfile()).isNotNull();
        assertThat(loginChanged.getProfile()
                .getId())
                .isNotNull()
                .isEqualTo(loginDAO.getProfile().getId());
        assertThat(loginChanged.getProfile().getDescription())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(loginDAO.getProfile().getDescription());
    }

    @Test
    void should_change_login_when_profile_was_change() {

        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name()));
        when(loginRepository.save(any(LoginDAO.class)))
                .thenReturn(Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_CHILD_LOGIN.name()));

        LoginDTO loginChanged = loginService.changeLogin(UUID.randomUUID(),
                Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_CHILD_LOGIN.name()));

        assertThat(loginChanged).isNotNull();
    }

    @Test
    void should_change_login_when_profile_was_change_but_is_just_a_profile_change() {

        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name()));
        when(loginRepository.save(any(LoginDAO.class)))
                .thenReturn(Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_CHILD_LOGIN.name()));

        LoginDTO loginChanged = loginService.changeLogin(UUID.randomUUID(),
                Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.INCOMPLETE_OWNER_LOGIN.name()));

        assertThat(loginChanged).isNotNull();
    }

    @Test
    void should_throw_a_not_found_exception_when_it_try_change_login_but_it_does_not_exist() {

        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(null);

        assertThrows(NotFoundException.class, () -> loginService.changeLogin(UUID.randomUUID(),
                Fixture.from(LoginDTO.class).gimme(LoginDTOTemplate.TYPES.VALID_CHILD_LOGIN.name())));
    }

    @Test
    void should_retrieve_a_login_when_it_receive_a_correctly_userId() {

        LoginDAO loginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(loginDAO);

        LoginDTO login = loginService.getLogin(USER_ID);

        assertThat(login).isNotNull();
        assertThat(login.getId())
                .isNotNull()
                .isEqualTo(loginDAO.getId());
        assertThat(login.getUserName())
                .isNotNull()
                .isEqualTo(loginDAO.getUserName());
        assertThat(login.getPassword())
                .isNotNull()
                .isEqualTo(loginDAO.getPassword());
        assertThat(login.getProfile().getId())
                .isNotNull()
                .isEqualTo(loginDAO.getProfile().getId());
        assertThat(login.getUserId()).isNull();
    }

    @Test
    void should_throw_an_exception_when_user_does_not_exist_during_try_get_an_user() {
        when(loginRepository.findBy(any(UUID.class)))
                .thenReturn(null);

        assertThrows(NotFoundException.class, () -> loginService.getLogin(USER_ID_INVALID));
    }

    @Test
    void should_retrieve_a_login_when_it_receive_a_correctly_userName() {
        LoginDAO loginDAO = Fixture.from(LoginDAO.class).gimme(LoginDAOTemplate.TYPES.VALID_OWNER_LOGIN.name());
        when(loginRepository.findBy(loginDAO.getUserName()))
                .thenReturn(loginDAO);

        LoginDTO login = loginService.getLoginByUserName(loginDAO.getUserName());

        assertThat(login).isNotNull();
        assertThat(login.getId())
                .isNotNull()
                .isEqualTo(loginDAO.getId());
        assertThat(login.getUserName())
                .isNotNull()
                .isEqualTo(loginDAO.getUserName());
        assertThat(login.getPassword())
                .isNotNull()
                .isEqualTo(loginDAO.getPassword());
        assertThat(login.getProfile().getId())
                .isNotNull()
                .isEqualTo(loginDAO.getProfile().getId());
        assertThat(login.getUserId())
                .isNotNull()
                .isEqualTo(loginDAO.getAccount().getId());
    }

    @Test
    void should_throw_an_exception_when_userName_does_not_exist_during_try_get_an_user() {
        when(loginRepository.findBy(anyString()))
                .thenReturn(null);

        assertThrows(NotFoundException.class, () -> loginService.getLoginByUserName(USER_NAME_INVALID));
    }
}