package com.tcc.account.service.service;

import br.com.six2six.fixturefactory.Fixture;
import com.tcc.account.service.base.BaseTest;
import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.CompanyDAO;
import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.repository.CompanyRepository;
import com.tcc.account.service.templates.CompanyDAOTemplate;
import com.tcc.account.service.templates.CompanyDTOTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class CompanyServiceTest extends BaseTest {

    private CompanyService companyService;

    @Mock
    private CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        openMocks(this);

        companyService = new CompanyService(companyRepository);
    }

    @Test
    void should_return_true_when_company_exists() {
        when(companyRepository.findBy(anyString()))
                .thenReturn(Fixture.from(CompanyDAO.class).gimme(CompanyDAOTemplate.TYPES.VALID.name()));

        boolean hasCompany = companyService.verifyIfCompanyExist("Ateliê Apareceu");

        assertThat(hasCompany).isTrue();
    }

    @Test
    void should_return_false_when_company_does_not_exists() {

        when(companyRepository.findBy(anyString()))
                .thenReturn(null);

        boolean hasCompany = companyService.verifyIfCompanyExist("Ateliê Desapareceu");

        assertThat(hasCompany).isFalse();
    }

    @Test
    void should_create_a_company_correctly() {
        CompanyDTO companyToCreate = Fixture.from(CompanyDTO.class).gimme(CompanyDTOTemplate.TYPES.VALID_TO_CREATE.name());

        when(companyRepository.save(any(CompanyDAO.class)))
                .thenReturn(Fixture.from(CompanyDAO.class).gimme(CompanyDAOTemplate.TYPES.VALID.name()));

        CompanyDTO company = companyService.createCompany(companyToCreate);

        assertThat(company).isNotNull();
        assertThat(company.getId()).isNotNull();
        assertThat(company.getName()).isNotNull().isNotEmpty().isNotBlank();
    }

    @Test
    void should_change_a_company_correctly() {
        CompanyDTO companyToChange = Fixture.from(CompanyDTO.class)
                .gimme(CompanyDTOTemplate.TYPES.VALID_TO_CHANGE.name());
        CompanyDAO companyDAOFixture = Fixture.from(CompanyDAO.class)
                .gimme(CompanyDAOTemplate.TYPES.VALID.name());

        when(companyRepository.findById(any(UUID.class)))
                .thenReturn(Optional.of(companyDAOFixture));

        when(companyRepository.save(any(CompanyDAO.class)))
                .thenReturn(companyDAOFixture);

        CompanyDTO company = companyService.changeCompany(companyToChange);

        assertThat(company).isNotNull();
        assertThat(company.getId())
                .isNotNull()
                .isEqualTo(companyDAOFixture.getId());
        assertThat(company.getName())
                .isNotNull()
                .isNotEmpty()
                .isNotBlank()
                .isEqualTo(companyDAOFixture.getName());
    }

    @Test
    void should_throw_an_exception_an_company_does_not_exist() {
        CompanyDTO companyToChange = Fixture.from(CompanyDTO.class)
                .gimme(CompanyDTOTemplate.TYPES.VALID_TO_CHANGE.name());

        when(companyRepository.findById(any(UUID.class)))
                .thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> companyService.changeCompany(companyToChange));
    }
}