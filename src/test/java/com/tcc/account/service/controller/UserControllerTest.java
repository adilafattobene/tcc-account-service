package com.tcc.account.service.controller;

import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.model.dto.SimpleUserDTO;
import com.tcc.account.service.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

class UserControllerTest {

    public static final UUID USER_ID_VALID = UUID.randomUUID();
    private UserController userController;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        openMocks(this);

        userController = new UserController(userService);
    }

    @Test
    void should_return_a_user_with_full_params_when_it_exists() {
        when(userService.getUserBy(USER_ID_VALID))
                .thenReturn(SimpleUserDTO.builder()
                        .id(USER_ID_VALID)
                        .name("Maria Aparecida Garcia")
                        .company(CompanyDTO.builder()
                                .id(UUID.randomUUID())
                                .name("Ateliê Apareceu")
                                .build())
                        .build());

        SimpleUserDTO simpleUserDTO = userController.getUserById(USER_ID_VALID);

        assertThat(simpleUserDTO.getId()).isEqualTo(USER_ID_VALID);
        assertThat(simpleUserDTO.getName()).isNotEmpty().isNotBlank();
        assertThat(simpleUserDTO.getCompany()).isNotNull();
        assertThat(simpleUserDTO.getCompany().getId()).isNotNull();
        assertThat(simpleUserDTO.getCompany().getName()).isNotEmpty().isNotBlank();
    }

    @Test
    void should_return_a_user_list_when__getUsersById() {

    }

    @Test
    void createUser() {
    }

    @Test
    void testCreateUser() {
    }


}