package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dao.ProfileDAO;

import java.util.UUID;

public class ProfileDAOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String DESCRIPTION = "description";

    public enum TYPES {
        VALID_CHILD,
        VALID_OWNER,
        VALID,
        VALID_TO_CREATE
    }

    @Override
    public void load() {
        buildValidOwner();
        buildValidChild();
        buildValid();
        buildValidToCreate();
    }

    private void buildValidOwner() {
        Fixture.of(ProfileDAO.class).addTemplate(ProfileDAOTemplate.TYPES.VALID_OWNER.name(), new Rule() {{
            add(ID, UUID.fromString("ffb1bff4-4606-4fae-adae-89647e95d01b"));
            add(DESCRIPTION, "OWNER");
        }});
    }

    private void buildValidChild() {
        Fixture.of(ProfileDAO.class).addTemplate(ProfileDAOTemplate.TYPES.VALID_CHILD.name(), new Rule() {{
            add(ID, UUID.fromString("6b6cfe56-8da1-4af0-bf36-f88d0764398e"));
            add(DESCRIPTION, "CHILD");
        }});
    }

    private void buildValidToCreate() {
        Fixture.of(ProfileDAO.class).addTemplate(TYPES.VALID_TO_CREATE.name(), new Rule() {{
            add(ID, UUID.randomUUID());
        }});
    }

    private void buildValid() {
        Fixture.of(ProfileDAO.class).addTemplate(TYPES.VALID.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(DESCRIPTION, "WHATEVER");
        }});
    }
}
