package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dao.CompanyDAO;

import java.util.UUID;

public class CompanyDAOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String NAME = "name";

    public enum TYPES {
        VALID
    }

    @Override
    public void load() {
        buildValid();
    }

    private void buildValid() {
        Fixture.of(CompanyDAO.class).addTemplate(CompanyDAOTemplate.TYPES.VALID.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Ateliê Apareceu");
        }});
    }
}
