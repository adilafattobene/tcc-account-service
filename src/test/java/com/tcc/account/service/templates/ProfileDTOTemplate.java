package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dto.ProfileDTO;

import java.util.UUID;

public class ProfileDTOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String DESCRIPTION = "description";

    public enum TYPES {
        VALID_CHILD,
        VALID_OWNER,
        VALID_TO_CREATE,
        VALID
    }

    @Override
    public void load() {
        buildValidOwner();
        buildValidChild();
        buildValidToCreate();
        buildValid();
    }

    private void buildValidOwner() {
        Fixture.of(ProfileDTO.class).addTemplate(ProfileDTOTemplate.TYPES.VALID_OWNER.name(), new Rule() {{
            add(ID, UUID.fromString("ffb1bff4-4606-4fae-adae-89647e95d01b"));
            add(DESCRIPTION, "OWNER");
        }});
    }

    private void buildValidChild() {
        Fixture.of(ProfileDTO.class).addTemplate(ProfileDTOTemplate.TYPES.VALID_CHILD.name(), new Rule() {{
            add(ID, UUID.fromString("6b6cfe56-8da1-4af0-bf36-f88d0764398e"));
            add(DESCRIPTION, "CHILD");
        }});
    }

    private void buildValidToCreate() {
        Fixture.of(ProfileDTO.class).addTemplate(TYPES.VALID_TO_CREATE.name(), new Rule() {{
            add(ID, UUID.randomUUID());
        }});
    }

    private void buildValid() {
        Fixture.of(ProfileDTO.class).addTemplate(TYPES.VALID.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(DESCRIPTION, "WHATEVER");
        }});
    }
}
