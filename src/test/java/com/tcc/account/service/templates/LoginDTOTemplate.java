package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.ProfileDTO;

import java.util.UUID;

public class LoginDTOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String PASSWORD = "password";
    private static final String USER_NAME = "userName";
    private static final String PROFILE = "profile";

    public enum TYPES {
        VALID_OWNER_LOGIN,
        INCOMPLETE_OWNER_LOGIN,
        VALID_CHILD_LOGIN,
        VALID_TO_CREATE,
        VALID_TO_CREATE_OWNER,
        VALID_TO_CHANGE
    }

    @Override
    public void load() {
        buildValidOwnerLogin();
        buildValidChildLogin();
        buildValidLoginToCreate();
        buildValidLoginToCreateOwner();
        buildValidOwnerLogin_just_profile();
        buildValidToChange();
    }

    private void buildValidToChange() {
        Fixture.of(LoginDTO.class).addTemplate(TYPES.VALID_TO_CHANGE.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, "$2y$12$KpldmB5OUXIMdfGfCN7I/uZeHAyAwlmr7i6.kpUfBm8SP0bAwcFkK ");
            add(USER_NAME, "simmechanic");
            add(PROFILE, one(ProfileDTO.class, ProfileDTOTemplate.TYPES.VALID_OWNER.name()));
        }});
    }

    private void buildValidOwnerLogin() {
        Fixture.of(LoginDTO.class).addTemplate(LoginDTOTemplate.TYPES.VALID_OWNER_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, "$2y$12$KpldmB5OUXIMdfGfCN7I/uZeHAyAwlmr7i6.kpUfBm8SP0bAwcFkK ");
            add(USER_NAME, "simmechanic");
            add(PROFILE, one(ProfileDTO.class, ProfileDTOTemplate.TYPES.VALID_OWNER.name()));
        }});
    }

    private void buildValidOwnerLogin_just_profile() {
        Fixture.of(LoginDTO.class).addTemplate(LoginDTOTemplate.TYPES.INCOMPLETE_OWNER_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, null);
            add(USER_NAME, null);
            add(PROFILE, one(ProfileDTO.class, ProfileDTOTemplate.TYPES.VALID_OWNER.name()));
        }});
    }

    private void buildValidChildLogin() {
        Fixture.of(LoginDTO.class).addTemplate(TYPES.VALID_CHILD_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, "$2y$12$eH83JGd9.Yh4m0e6Q04BWuwvMbOnTZ6.11BGnbRKV22sg4MSigaj. ");
            add(USER_NAME, "simhost");
            add(PROFILE, one(ProfileDTO.class, ProfileDTOTemplate.TYPES.VALID_CHILD.name()));
        }});
    }

    private void buildValidLoginToCreate() {
        Fixture.of(LoginDTO.class).addTemplate(TYPES.VALID_TO_CREATE.name(), new Rule() {{
            add(PASSWORD, "$2y$12$eH83JGd9.Yh4m0e6Q04BWuwvMbOnTZ6.11BGnbRKV22sg4MSigaj. ");
            add(USER_NAME, "aquaique");
            add(PROFILE, one(ProfileDTO.class, ProfileDTOTemplate.TYPES.VALID_TO_CREATE.name()));
        }});
    }

    private void buildValidLoginToCreateOwner() {
        Fixture.of(LoginDTO.class).addTemplate(TYPES.VALID_TO_CREATE_OWNER.name(), new Rule() {{
            add(PASSWORD, "$2y$12$Wejy.NtGj8ASZPeiAKmf7u0Zl0qKBTV9HnQbdmArgZie8uKM19WRy ");
            add(USER_NAME, "pancakestheapartment");
        }});
    }
}
