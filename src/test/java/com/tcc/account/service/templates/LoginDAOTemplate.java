package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dao.AccountDAO;
import com.tcc.account.service.model.dao.LoginDAO;
import com.tcc.account.service.model.dao.ProfileDAO;

import java.util.UUID;

public class LoginDAOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String PASSWORD = "password";
    private static final String USER_NAME = "userName";
    private static final String PROFILE = "profile";
    private static final String ACCOUNT = "account";


    public enum TYPES {
        VALID_OWNER_LOGIN,
        VALID_CHILD_LOGIN,
        VALID_TO_CREATE,
    }

    @Override
    public void load() {
        buildValidOwnerLogin();
        buildValidChildLogin();
        buildValidLoginToCreate();
    }

    private void buildValidOwnerLogin() {
        Fixture.of(LoginDAO.class).addTemplate(TYPES.VALID_OWNER_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, "$2y$12$KpldmB5OUXIMdfGfCN7I/uZeHAyAwlmr7i6.kpUfBm8SP0bAwcFkK ");
            add(USER_NAME, "simmechanic");
            add(PROFILE, one(ProfileDAO.class, ProfileDAOTemplate.TYPES.VALID_OWNER.name()));
            add(ACCOUNT, one(AccountDAO.class, AccountDAOTemplate.TYPES.VALID_OWNER.name()));
        }});
    }

    private void buildValidChildLogin() {
        Fixture.of(LoginDAO.class).addTemplate(TYPES.VALID_CHILD_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(PASSWORD, "$2y$12$eH83JGd9.Yh4m0e6Q04BWuwvMbOnTZ6.11BGnbRKV22sg4MSigaj. ");
            add(USER_NAME, "simhost");
            add(PROFILE, one(ProfileDAO.class, ProfileDAOTemplate.TYPES.VALID_CHILD.name()));
            add(ACCOUNT, one(AccountDAO.class, AccountDAOTemplate.TYPES.VALID_CHILD.name()));
        }});
    }

    private void buildValidLoginToCreate() {
        Fixture.of(LoginDAO.class).addTemplate(TYPES.VALID_TO_CREATE.name(), new Rule() {{
            add(PASSWORD, "$2y$12$eH83JGd9.Yh4m0e6Q04BWuwvMbOnTZ6.11BGnbRKV22sg4MSigaj. ");
            add(USER_NAME, "aquaique");
            add(PROFILE, one(ProfileDAO.class, ProfileDAOTemplate.TYPES.VALID_TO_CREATE.name()));
        }});
    }
}
