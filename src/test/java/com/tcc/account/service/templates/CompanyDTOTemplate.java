package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dto.CompanyDTO;

import java.util.UUID;

public class CompanyDTOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String NAME = "name";

    public enum TYPES {
        VALID,
        VALID_TO_CREATE,
        VALID_TO_CHANGE
    }

    @Override
    public void load() {
        buildValid();
        buildValidToCreate();
        buildValidToChange();
    }

    private void buildValid() {
        Fixture.of(CompanyDTO.class).addTemplate(CompanyDTOTemplate.TYPES.VALID.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Aplicado Casa");
        }});
    }

    private void buildValidToCreate() {
        Fixture.of(CompanyDTO.class).addTemplate(CompanyDTOTemplate.TYPES.VALID_TO_CREATE.name(), new Rule() {{
            add(NAME, "Robin Petshop");
        }});
    }

    private void buildValidToChange() {
        Fixture.of(CompanyDTO.class).addTemplate(TYPES.VALID_TO_CHANGE.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Nami Maravilhosa Ltda");
        }});
    }
}
