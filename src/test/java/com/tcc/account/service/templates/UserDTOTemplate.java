package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.UserDTO;

import java.util.UUID;

public class UserDTOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String COMPANY = "company";
    private static final String LOGIN = "login";

    public enum TYPES {
        VALID,
        VALID_TO_CREATE_OWNER,
        VALID_TO_CREATE_CHILD,
        INVALID_COMPANY_TO_CHANGE_USER,
        VALID_USER_TO_CHANGE_NAME,
        VALID_USER_TO_CHANGE_COMPANY,
        VALID_USER_TO_CHANGE_LOGIN
    }

    @Override
    public void load() {
        buildValid();
        buildValidToCreateOwnerUser();
        buildValidToCreateChildUser();
        buildUserToChangeWithAnInvalidCompany();
        buildValidUserToChangeName();
        buildValidUserToChangeLogin();
        buildValidUserToChangeCompany();
    }

    private void buildValid() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Viviane Curvelo Baldaia");
            add(COMPANY, one(CompanyDTO.class, CompanyDTOTemplate.TYPES.VALID.name()));
            add(LOGIN, one(LoginDTO.class, LoginDTOTemplate.TYPES.VALID_OWNER_LOGIN.name()));
        }});
    }

    private void buildValidToCreateOwnerUser() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID_TO_CREATE_OWNER.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Maria Aparecida Garcia");
            add(COMPANY, one(CompanyDTO.class, CompanyDTOTemplate.TYPES.VALID_TO_CREATE.name()));
            add(LOGIN, one(LoginDTO.class, LoginDTOTemplate.TYPES.VALID_TO_CREATE.name()));
        }});
    }

    private void buildValidToCreateChildUser() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID_TO_CREATE_CHILD.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Jandira Ferrão Marinho");
            add(COMPANY, one(CompanyDTO.class, CompanyDTOTemplate.TYPES.VALID_TO_CREATE.name()));
            add(LOGIN, one(LoginDTO.class, LoginDTOTemplate.TYPES.VALID_TO_CREATE.name()));
        }});
    }

    private void buildUserToChangeWithAnInvalidCompany() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.INVALID_COMPANY_TO_CHANGE_USER.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Maria Aparecida Garcia");
            add(COMPANY, CompanyDTO.builder().build());
            add(LOGIN, one(LoginDTO.class, LoginDTOTemplate.TYPES.VALID_TO_CHANGE.name()));
        }});
    }

    private void buildValidUserToChangeName() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID_USER_TO_CHANGE_NAME.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Maria Aparecida Garcia");
            add(COMPANY, null);
            add(LOGIN, null);
        }});
    }

    private void buildValidUserToChangeCompany() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID_USER_TO_CHANGE_COMPANY.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, null);
            add(COMPANY, one(CompanyDTO.class, CompanyDTOTemplate.TYPES.VALID_TO_CHANGE.name()));
            add(LOGIN, null);
        }});
    }

    private void buildValidUserToChangeLogin() {
        Fixture.of(UserDTO.class).addTemplate(TYPES.VALID_USER_TO_CHANGE_LOGIN.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, null);
            add(COMPANY, null);
            add(LOGIN, one(LoginDTO.class, LoginDTOTemplate.TYPES.VALID_TO_CHANGE.name()));
        }});
    }
}
