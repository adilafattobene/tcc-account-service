package com.tcc.account.service.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.tcc.account.service.model.dao.AccountDAO;
import com.tcc.account.service.model.dao.CompanyDAO;

import java.util.UUID;

public class AccountDAOTemplate implements TemplateLoader {

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String COMPANY = "company";
    private static final String OWNER = "owner";

    public enum TYPES {
        VALID_OWNER,
        VALID_CHILD
    }

    @Override
    public void load() {
        buildValidOwner();
        buildValidChild();
    }

    private void buildValidOwner() {
        Fixture.of(AccountDAO.class).addTemplate(AccountDAOTemplate.TYPES.VALID_OWNER.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Maria Aparecida Garcia");
            add(COMPANY, one(CompanyDAO.class, CompanyDAOTemplate.TYPES.VALID.name()));
        }});
    }

    private void buildValidChild() {
        Fixture.of(AccountDAO.class).addTemplate(AccountDAOTemplate.TYPES.VALID_CHILD.name(), new Rule() {{
            add(ID, UUID.randomUUID());
            add(NAME, "Maria Aparecida Garcia");
            add(COMPANY, one(CompanyDAO.class, CompanyDAOTemplate.TYPES.VALID.name()));
            add(OWNER, one(AccountDAO.class, TYPES.VALID_OWNER.name()));
        }});
    }
}
