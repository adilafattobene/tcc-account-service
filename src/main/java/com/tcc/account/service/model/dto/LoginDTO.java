package com.tcc.account.service.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginDTO {

    private UUID id;
    private String password;
    private String userName;
    private ProfileDTO profile;
    private UUID userId;
}
