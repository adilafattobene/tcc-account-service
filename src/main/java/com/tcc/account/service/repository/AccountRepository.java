package com.tcc.account.service.repository;

import com.tcc.account.service.model.dao.AccountDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<AccountDAO, UUID> {

    @Query(value = "select * from account where owner_id = :ownerId", nativeQuery = true)
    List<AccountDAO> findAllUsersById(@Param("ownerId") UUID ownerId);
}
