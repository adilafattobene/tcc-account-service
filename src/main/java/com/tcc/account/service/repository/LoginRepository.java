package com.tcc.account.service.repository;

import com.tcc.account.service.model.dao.LoginDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface LoginRepository extends JpaRepository<LoginDAO, UUID> {

    @Query(value = "select l.* from login l where l.user_name = :userName", nativeQuery = true)
    LoginDAO findByUserName(@Param("userName") String userName);

    @Query(value = "select l.* from login l where l.account_id = :accountId", nativeQuery = true)
    LoginDAO findBy(@Param("accountId") UUID accountId);

    @Query(value = "select l.* from login l where l.user_name = :userName", nativeQuery = true)
    LoginDAO findBy(@Param("userName") String userName);
}
