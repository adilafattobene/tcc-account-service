package com.tcc.account.service.repository;

import com.tcc.account.service.model.dao.ProfileDAO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProfileRepository extends JpaRepository<ProfileDAO, UUID> {
}
