package com.tcc.account.service.repository;

import com.tcc.account.service.model.dao.CompanyDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.UUID;

public interface CompanyRepository extends JpaRepository<CompanyDAO, UUID> {

    @Query(value = "select c.* from company c where c.name = :companyName", nativeQuery = true)
    CompanyDAO findBy(@Param("companyName") String companyName);
}
