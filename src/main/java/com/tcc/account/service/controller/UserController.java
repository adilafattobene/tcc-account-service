package com.tcc.account.service.controller;

import com.tcc.account.service.model.dto.SimpleUserDTO;
import com.tcc.account.service.model.dto.UserDTO;
import com.tcc.account.service.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public SimpleUserDTO getUserById(@PathVariable UUID id) {
        return userService.getUserBy(id);
    }

    @GetMapping("/{id}/users")
    public List<UserDTO> getUsersById(@PathVariable UUID id) {
        return userService.getUsersById(id);
    }

    @PostMapping()
    public UserDTO createSubUser(@RequestBody UserDTO user) {
        return userService.createUser(user);
    }

    @PostMapping("/{id}")
    public UserDTO createSubUser(@RequestBody UserDTO user, @PathVariable UUID id) {
        return userService.createSubUser(user, id);
    }

    @PutMapping("/{id}")
    public UserDTO changeUser(@RequestBody UserDTO user, @PathVariable UUID id) {
        return userService.changeUser(user, id);
    }
}
