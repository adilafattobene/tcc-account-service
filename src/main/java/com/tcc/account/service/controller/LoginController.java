package com.tcc.account.service.controller;

import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.service.LoginService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/login")
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PutMapping("/user/{id}")
    public LoginDTO changeLogin(@PathVariable UUID id, @RequestBody LoginDTO login) {
        return loginService.changeLogin(id, login);
    }

    @GetMapping("/user/{id}")
    public LoginDTO getLogin(@PathVariable UUID id) {
        return loginService.getLogin(id);
    }

    @GetMapping("/user")
    public LoginDTO getLoginByUserName(@RequestParam String userName) {
        return loginService.getLoginByUserName(userName);
    }
}