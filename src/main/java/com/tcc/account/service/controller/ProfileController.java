package com.tcc.account.service.controller;

import com.tcc.account.service.model.dto.ProfileDTO;
import com.tcc.account.service.service.ProfileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final ProfileService profileService;

    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @GetMapping()
    public List<ProfileDTO> getAll() {
        return profileService.getProfiles();
    }

    @GetMapping("/{id}")
    public ProfileDTO getProfile(@PathVariable UUID id) {
        return profileService.getProfileById(id);
    }

    @GetMapping("/user/{id}")
    public ProfileDTO getUserProfile(@PathVariable UUID id) {
        return profileService.getUserProfileByUserId(id);
    }
}
