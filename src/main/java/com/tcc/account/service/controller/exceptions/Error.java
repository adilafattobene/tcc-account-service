package com.tcc.account.service.controller.exceptions;

import lombok.Getter;

@Getter
public class Error {
    private final String message;
    private final String error;

    public Error(String message, String error) {
        this.message = message;
        this.error = error;
    }
}
