package com.tcc.account.service.controller.exceptions;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public final class RestExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public Error notFoundError(final Exception e, final WebRequest request) {
        return new Error(e.getMessage(), "resource_not_found_error");
    }

    @ExceptionHandler(ConflictException.class)
    @ResponseStatus(CONFLICT)
    public Error conflict(final Exception e, final WebRequest request) {
        return new Error(e.getMessage(), "conflict_error");
    }
}
