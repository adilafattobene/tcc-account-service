package com.tcc.account.service.service;

import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.CompanyDAO;
import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }


    public boolean verifyIfCompanyExist(String companyName) {

        return Optional.ofNullable(companyRepository.findBy(companyName)).isPresent();
    }

    public CompanyDTO createCompany(CompanyDTO build) {

        CompanyDAO companyDAO = new CompanyDAO();
        companyDAO.setName(build.getName());

        CompanyDAO companySaved = companyRepository.save(companyDAO);

        return CompanyDTO.builder()
                .id(companySaved.getId())
                .name(companySaved.getName())
                .build();
    }

    @Transactional
    public CompanyDTO changeCompany(CompanyDTO build) {

        CompanyDAO companyDAO = companyRepository.findById(build.getId())
                .orElseThrow(() -> new NotFoundException("Company not found during the process to change company Id: "
                        + build.getId().toString()));

        companyDAO.setName(build.getName());

        CompanyDAO companyChanged = companyRepository.save(companyDAO);

        return CompanyDTO.builder()
                .id(companyChanged.getId())
                .name(companyChanged.getName())
                .build();
    }
}




