package com.tcc.account.service.service;

import com.tcc.account.service.controller.exceptions.ConflictException;
import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.AccountDAO;
import com.tcc.account.service.model.dao.CompanyDAO;
import com.tcc.account.service.model.dto.CompanyDTO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.SimpleUserDTO;
import com.tcc.account.service.model.dto.UserDTO;
import com.tcc.account.service.repository.AccountRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final AccountRepository accountRepository;
    private final CompanyService companyService;
    private final LoginService loginService;

    public UserService(AccountRepository accountRepository, CompanyService companyService, LoginService loginService) {
        this.accountRepository = accountRepository;
        this.companyService = companyService;
        this.loginService = loginService;
    }

    public SimpleUserDTO getUserBy(UUID id) {
        AccountDAO account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Usuário não encontrado"));

        return SimpleUserDTO.builder()
                .id(account.getId())
                .name(account.getName())
                .company(CompanyDTO.builder()
                        .id(account.getCompany().getId())
                        .name(account.getCompany().getName()).build())
                .build();
    }

    @Transactional
    public List<UserDTO> getUsersById(UUID id) {
        List<AccountDAO> children = accountRepository.findAllUsersById(id);

        return children.stream()
                .map(a -> {
                    LoginDTO loginById = loginService.getLoginById(a.getId());
                    return UserDTO.builder()
                            .id(a.getId())
                            .name(a.getName())
                            .login(LoginDTO.builder()
                                    .userName(loginById.getUserName())
                                    .profile(loginById.getProfile())
                                    .build())
                            .build();
                })
                .collect(Collectors.toList());
    }

    public UserDTO createUser(UserDTO user) {
        CompanyDTO company = createCompanyDTO(user);

        AccountDAO accountSaved = createAccountDAO(user, company);

        LoginDTO login = loginService.createLogin(user.getLogin(), accountSaved.getId());

        return UserDTO.builder()
                .id(accountSaved.getId())
                .name(accountSaved.getName())
                .login(login)
                .company(company)
                .build();
    }

    private AccountDAO createAccountDAO(UserDTO user, CompanyDTO company) {
        CompanyDAO companyDAO = new CompanyDAO();
        companyDAO.setId(company.getId());

        AccountDAO account = new AccountDAO();
        account.setCompany(companyDAO);
        account.setName(user.getName());

        return accountRepository.save(account);
    }

    private CompanyDTO createCompanyDTO(UserDTO user) {
        if (companyService.verifyIfCompanyExist(user.getCompany().getName())) {
            throw new ConflictException("Já existe uma Company com esse nome.");
        }

        return companyService.createCompany(user.getCompany());
    }

    @Transactional
    public UserDTO createSubUser(UserDTO child, UUID ownerId) {

        Optional<AccountDAO> ownerInDBA = accountRepository.findById(ownerId);

        if (ownerInDBA.isEmpty()) {
            throw new NotFoundException("Não foi encontrado o usuário pai");
        }

        AccountDAO childToSave = new AccountDAO();
        childToSave.setName(child.getName());
        childToSave.setOwner(ownerInDBA.get());
        childToSave.setCompany(ownerInDBA.get().getCompany());

        AccountDAO childSaved = accountRepository.save(childToSave);

        LoginDTO login = loginService.createLogin(LoginDTO.builder()
                        .userName(child.getLogin().getUserName())
                        .password(child.getLogin().getPassword())
                        .profile(child.getLogin().getProfile())
                        .build(),
                childSaved.getId());

        return UserDTO.builder()
                .id(childSaved.getId())
                .name(childSaved.getName())
                .login(login)
                .company(CompanyDTO.builder()
                        .id(ownerInDBA.get().getId())
                        .build())
                .build();
    }

    @Transactional
    public UserDTO changeUser(UserDTO user, UUID id) {
        AccountDAO account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Usuário não encontrado"));


        if (Optional.ofNullable(user.getName()).isPresent()) {
            account.setName(user.getName());
        }

        if (Optional.ofNullable(user.getCompany()).isPresent()) {
            CompanyDAO companyDAO = getCompanyDAOToChange(user);

            account.setCompany(companyDAO);
        }

        if (Optional.ofNullable(user.getLogin()).isPresent()) {
            loginService.changeLogin(account.getId(), user.getLogin());
        }

        AccountDAO save = accountRepository.save(account);

        return UserDTO.builder()
                .name(save.getName())
                .id(save.getId())
                .company(CompanyDTO.builder()
                        .id(save.getCompany().getId())
                        .name(save.getCompany().getName())
                        .build())
                .build();
    }

    private CompanyDAO getCompanyDAOToChange(UserDTO user) {
        if (Optional.ofNullable(user.getCompany().getId()).isEmpty() ||
                Optional.ofNullable(user.getCompany().getName()).isEmpty()) {
            throw new IllegalArgumentException("Company inválida.");
        }

        CompanyDTO companyCopy = companyService.changeCompany(CompanyDTO.builder()
                .id(user.getCompany().getId())
                .name(user.getCompany().getName())
                .build());

        CompanyDAO companyDAO = new CompanyDAO();
        companyDAO.setId(companyCopy.getId());
        companyDAO.setName(companyCopy.getName());
        return companyDAO;
    }
}
