package com.tcc.account.service.service;

import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.ProfileDAO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.ProfileDTO;
import com.tcc.account.service.repository.ProfileRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProfileService {

    private final ProfileRepository profileRepository;
    private final LoginService loginService;

    public ProfileService(ProfileRepository profileRepository, LoginService loginService) {
        this.profileRepository = profileRepository;
        this.loginService = loginService;
    }

    public List<ProfileDTO> getProfiles() {

        return profileRepository.findAll().stream()
                .map(profile -> ProfileDTO.builder()
                        .id(profile.getId())
                        .description(profile.getDescription())
                        .build()).collect(Collectors.toList());
    }

    public ProfileDTO getProfileById(UUID id) {
        ProfileDAO profileDAO = profileRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Profile não encontrado"));

        return ProfileDTO.builder()
                .id(profileDAO.getId())
                .description(profileDAO.getDescription())
                .build();
    }

    public ProfileDTO getUserProfileByUserId(UUID userId) {
        LoginDTO loginById = Optional.ofNullable(loginService.getLoginById(userId))
                .orElseThrow(() -> new NotFoundException(String.format("Usuário não encontrado - userId: ", userId)));

        return ProfileDTO.builder()
                .id(loginById.getProfile().getId())
                .description(loginById.getProfile().getDescription())
                .build();
    }
}
