package com.tcc.account.service.service;

import com.tcc.account.service.controller.exceptions.NotFoundException;
import com.tcc.account.service.model.dao.AccountDAO;
import com.tcc.account.service.model.dao.LoginDAO;
import com.tcc.account.service.model.dao.ProfileDAO;
import com.tcc.account.service.model.dto.LoginDTO;
import com.tcc.account.service.model.dto.ProfileDTO;
import com.tcc.account.service.repository.LoginRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
public class LoginService {

    private final LoginRepository loginRepository;

    public LoginService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public LoginDTO createLogin(LoginDTO login, UUID accountId) {

        LoginDAO loginDAO = new LoginDAO();
        loginDAO.setPassword(login.getPassword());
        loginDAO.setUserName(login.getUserName());

        AccountDAO accountDAO = new AccountDAO();
        accountDAO.setId(accountId);
        loginDAO.setAccount(accountDAO);

        ProfileDAO profileDAO = new ProfileDAO();

        if (Optional.ofNullable(login.getProfile()).isPresent()) {
            profileDAO.setId(login.getProfile().getId());
        } else {
            profileDAO.setId(UUID.fromString("fcec55dc-9d24-4c0d-99ad-c99960660f2c"));
        }

        loginDAO.setProfile(profileDAO);

        LoginDAO save = loginRepository.save(loginDAO);

        return LoginDTO.builder()
                .id(save.getId())
                .userName(save.getUserName())
                .profile(ProfileDTO.builder()
                        .id(save.getProfile().getId())
                        .description(save.getProfile().getDescription())
                        .build())
                .build();
    }

    public LoginDTO getLoginById(UUID userId) {
        Optional<LoginDAO> by = Optional.ofNullable(loginRepository.findBy(userId));

        if (by.isEmpty()) {
            throw new NotFoundException(String.format("Não existe login para o usuário %s", userId));
        }

        return LoginDTO.builder()
                .id(by.get().getId())
                .userName(by.get().getUserName())
                .profile(ProfileDTO.builder()
                        .id(by.get().getProfile().getId())
                        .description(by.get().getProfile().getDescription())
                        .build())
                .build();
    }

    @Transactional
    public LoginDTO changeLogin(UUID userId, LoginDTO login) {

        Optional<LoginDAO> by = Optional.ofNullable(loginRepository.findBy(userId));

        if (by.isEmpty()) {
            throw new NotFoundException("Usuário não existe");
        }

        LoginDAO loginIntoDatabase = by.get();

        if (Optional.ofNullable(login.getUserName()).isPresent()) {
            loginIntoDatabase.setUserName(login.getUserName());
        }

        if (Optional.ofNullable(login.getPassword()).isPresent()) {
            loginIntoDatabase.setPassword(login.getPassword());
        }

        if (Optional.ofNullable(login.getProfile()).isPresent() &&
                Optional.ofNullable(login.getProfile().getId()).isPresent() &&
                !loginIntoDatabase.getProfile().getId()
                        .equals(login.getProfile().getId())) {
            ProfileDAO profileDAO = new ProfileDAO();
            profileDAO.setId(login.getId());

            loginIntoDatabase.setProfile(profileDAO);
        }

        LoginDAO save = loginRepository.save(loginIntoDatabase);

        return LoginDTO.builder()
                .id(save.getId())
                .userName(save.getUserName())
                .profile(ProfileDTO.builder()
                        .id(save.getProfile().getId())
                        .description(save.getProfile().getDescription())
                        .build())
                .build();
    }

    public LoginDTO getLogin(UUID id) {
        LoginDAO save = Optional.ofNullable(loginRepository.findBy(id))
                .orElseThrow(() -> new NotFoundException("Usuário não existe"));

        return LoginDTO.builder()
                .id(save.getId())
                .password(save.getPassword())
                .userName(save.getUserName())
                .profile(ProfileDTO.builder()
                        .id(save.getProfile().getId())
                        .description(save.getProfile().getDescription())
                        .build())
                .build();
    }

    public LoginDTO getLoginByUserName(String userName) {
        LoginDAO loginDAO = Optional.ofNullable(loginRepository.findBy(userName))
                .orElseThrow(() -> new NotFoundException(String.format("Não existe login cadastrado com o userName %s", userName)));

        return LoginDTO.builder()
                .id(loginDAO.getId())
                .password(loginDAO.getPassword())
                .userName(loginDAO.getUserName())
                .userId(loginDAO.getAccount().getId())
                .profile(ProfileDTO.builder()
                        .id(loginDAO.getProfile().getId())
                        .description(loginDAO.getProfile().getDescription())
                        .build())
                .build();

    }
}
